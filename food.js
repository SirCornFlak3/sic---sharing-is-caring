let registry;
const itemsDiv = document.querySelector("#items");

const JSONPath = "./data.dat.json";

async function getData(path){
    let fetching = await fetch(path);
    let response = await fetching.json();
    return response;
}

getData(JSONPath).then(d => {
registry = d;

for (food of d){
    console.log(food);
    const reservedClass = food.is_reserved ? "disabled" : "";
    const name = food.name;
    food.timestamp = Date(food.timestamp);
    itemsDiv.innerHTML = itemsDiv.innerHTML + `
    <div>
        <button class="collapsible-header">${name}</button>
        <div class="collapsible-body" style="display: none;">
            <span>
                <button class="buy-btn" ${reservedClass}>Erwerben</button>
                <br>
                ${food.location.human_readable_address}
            </span>
        </div>
    </div>`;
}

for (e of document.querySelectorAll(".buy-btn")){
    e.addEventListener("click", function (ev) {
        this.disabled = "true";
    })
}

for (e of document.querySelectorAll(".collapsible-header")) {
  e.addEventListener("click", function() {
    this.classList.toggle("active");
    let content = this.nextElementSibling;
    if (content.style.display == "none"){
        content.style.display = "block";
    } else {
        content.style.display = "none"
    } 
  });
}

document.querySelector("#loading").style.display = "none";
});
